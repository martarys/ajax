var xhr = new XMLHttpRequest();



xhr.open("GET", "o-nas.html", true);



//rejestrujemy zdarzenie - zmianę ready state
xhr.onreadystatechange = function(e){


    if( this.readyState === 4 && this.status === 200){

        console.log(this.response);
    }

};


//funkcja pokazująca w console inne zdarzenia
function logType(e){

    console.log(e.type);

}

//ustawiamy timeout na 1 milisekundę - ile możemy czekać na odpowiedź z serwera zanim 
//wykona się ontimeout
xhr.timeout = 1

//zdarzenia, które możemy obsłużyć
xhr.onloadstart = logType;
xhr.onprogress = logType;
xhr.onabort = logType;
xhr.onerror = logType;
xhr.onload = logType;
xhr.ontimeout = logType;
xhr.onloadend = logType;



xhr.send(null);

// console.log( xhr.response );

// UNSENT = 0
// OPENED = 1
// HEADERS_RECEIVED = 2
// LOADING = 3
// DONE = 4